function checkValid(str) {
    let adj = false; // set adjacent
    let ics = true; // set only increasing

    // loop str data
    for (let i = 0; i < str.length; i++) {
        if (str[i] == str[i+1]) {
            adj = true;
        }

        if (Number(str[i]) > Number(str[i+1])) {
            ics = false;
        }
    }

    let result = adj && ics;
    return result;
}

function test(input) {
    let spl = input.split('-').map(Number); // split data and convert string to number
    let counter = 0;

    let spl1 = spl[0]; // index 0
    let spl2 = spl[1]; // index 1

    // loop data spl
    for (let i = spl1; i <= spl2; ++i) {
        if (checkValid(`${i}`)) {
            counter++;
        }
    }

    return counter;
}

console.log(test('172851-675869'))